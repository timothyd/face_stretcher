#!/usr/bin/env python
import json
import requests
import time
import pandas as pd
import numpy as np
from datetime import timedelta
import datetime
import io
from pytz import timezone
import flask
from flask import send_file
import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from matplotlib import image
import dash_daq as daq
import dash_table
import plotly
from scipy import ndimage, misc
from skimage.transform import rescale
import itertools
import plotly.graph_objs as go
from base64 import decodestring
import base64
import cv2
from imageio import imread
from io import BytesIO
import os
from PIL import Image
from scipy import interpolate

global point_list
point_list = [0,0,0,0]

external_stylesheets = [dbc.themes.DARKLY]


server = flask.Flask(__name__)
app = dash.Dash(__name__, external_stylesheets=external_stylesheets, server=server)

app.config['suppress_callback_exceptions'] = True

default_graph = {
    'data': [],
    'layout': go.Layout(
        xaxis={
            'title': '',
            'type': 'linear',
            'showgrid':False,
            'visible':False,
            'ticks':'',
            'showticklabels':False
        },
        yaxis={
            'title': '',
            'type': 'linear',
            'side':'left',
            'visible':False,
            'showgrid':False,
            'ticks':'',
            'showticklabels':False
        },
        plot_bgcolor='rgb(40, 40, 40)',
        paper_bgcolor='rgb(40, 40, 40)',
        font={'color':'white'},
        margin={'l': 0, 'b': 1, 't': 1, 'r': 1},
        hovermode='closest',
    ),
}

app.layout = html.Div([
    html.Div(id='dummy3'),
    dcc.Loading(children=
        [
            html.Div(id='dummy2'),
            html.Div(children=
                [
                    dcc.Upload(
                        id='upload-image',
                        children=html.Div([
                            'Drag and Drop or ',
                            html.A('Select Files')
                        ]),
                        style={
                            'width': '100%',
                            'height': '60px',
                            'lineHeight': '60px',
                            'borderWidth': '1px',
                            'borderStyle': 'dashed',
                            'borderRadius': '5px',
                            'textAlign': 'center',
                            'margin': '10px'
                        },                                                                                                                                             
                    ),
                ]
            )
        ], type='circle'
    ),
    html.Div([
        html.Div([
            html.Div([
                dbc.Button("Left", id='left_button', color="primary", className="mr-1", style={'marginLeft':10,'marginBottom':10}),
                dbc.Button("Right", id='right_button', color="primary", className="mr-1", style={'marginLeft':10,'marginBottom':10}),
            ], style={'width':'10%', 'display':'inline-block'}),
            html.Div([
                dbc.Button("Nose", id='nose_button', color="primary", className="mr-1", style={'marginLeft':10,'marginBottom':10}),
                dbc.Button("Chin", id='chin_button', color="primary", className="mr-1", style={'marginLeft':10,'marginBottom':10}),
            ], style={'width':'10%', 'display':'inline-block'}),
            html.Div([
                dbc.Input(
                    id='rotation', 
                    type="number", 
                    min=-180, 
                    max=180, 
                    step=90, 
                    placeholder='rotation',
                    style={'marginLeft':10,'marginBottom':10, 'width':'100%', 'display':'inline-block'}),
                dcc.Dropdown(
                    id='padding',
                    options=[
                        {'label': '5%', 'value': 0.05},
                        {'label': '10%', 'value': 0.1},
                        {'label': '15%', 'value': 0.15},
                        {'label': '20%', 'value': 0.20},
                        {'label': '25%', 'value': 0.25}
                    ],
                    value=0.1,
                    style={'marginLeft':5,'marginBottom':10, 'width':'100%', 'display':'inline-block', 'color':'black'},
                ),
            ], style={'width':'10%', 'display':'inline-block'}),
            html.Div([
                dcc.Dropdown(
                    id='size',
                    options=[
                        {'label': 'Small', 'value': 'S'},
                        {'label': 'Medium', 'value': 'M'},
                        {'label': 'Large', 'value': 'L'},
                    ],
                    value='M',
                    style={'marginLeft':10,'marginBottom':10, 'width':'90%', 'display':'inline-block', 'color':'black'},
                ),
            ], style={'width':'20%', 'display':'inline-block'}),
            html.Div([
                dbc.Button("Process", id='process', color="primary", className="mr-1", style={'marginLeft':10,'marginBottom':10}),
            ], style={'width':'10%', 'display':'inline-block'}),
            html.Div([
            ], style={'width':'10%', 'display':'inline-block'}),
        ], style={'width':'100%','height':'17%','display':'inline-block'}),
        html.Div([
            html.Div([
                dcc.Graph(id='orig_graph', figure=default_graph, style={'display':'inline-block','width':'100%'}),
            ], style={'display':'inline-block', 'width':'100%','height':'auto'}),
        ], style={'width':'100%', 'height':'79%', 'marginTop':10})
    ], style={'width':'49%', 'height':'70vh','display':'inline-block', 'marginLeft':10}
    ),
    html.Div([
        html.Div([
            html.Div([
                html.P('Processed', style={'marginTop':10,'marginLeft':10}),
            ], style={'display':'inline-block','textAlign':'left','width':'49%'}),
            html.Div([
                html.A('Download', href='/downloads', style={'display':'inline-block','textAlign':'right'})
            ], style={'display':'inline-block','textAlign':'right','width':'49%'})
        ], style={'width':'100%','display':'inline-block','height':'17%'}),
        html.Div([
            dcc.Loading(children=
                [
                    html.Img(
                        id='stretched_image',
                        style={'height':'37%', 'width':'100%', 'display':'inline-block','verticalAlign':'top'}
                    ),
                ], type='circle'
            ),
        ], style={'width':'100%','display':'inline-block'})
    ], style={'width':'46%','display':'inline-block','verticalAlign':'top', 'height':'70vh'})
], style={'width':'100%', 'display':'inline-block'})



def make_picture(img, image, vx, vy):
    fig = go.Figure()
    # Constants
    img_width = img.shape[1]
    img_height = img.shape[0]
    scale_factor = 650/img_width
    # Add invisible scatter trace.
    # This trace is added to help the autoresize logic work.
    fig.add_trace(
        go.Scatter(
            x=[0, img_width * scale_factor],
            y=[0, img_height * scale_factor],
            mode="markers",
            marker_opacity=0
        )
    )
    #Add sigmoid
    fig.add_trace(
        go.Scatter(
            x=vy * img.shape[1] * scale_factor,
            y=vx,
            mode="lines",
            line={
                'color':'blue',
                'width':1
            },
        )
    )
    #Add left trace
    fig.add_trace(
        go.Scatter(
            x=[point_list[0], point_list[0]],
            y=[0, img_height * scale_factor],
            mode="lines",
            line={
                'color':'red'
            },
        )
    )
    #Add right trace
    fig.add_trace(
        go.Scatter(
            x=[point_list[1], point_list[1]],
            y=[0, img_height * scale_factor],
            mode="lines",
        )
    )
    #Add nose trace
    fig.add_trace(
        go.Scatter(
            x=[0, img_width * scale_factor],
            y=[point_list[2], point_list[2]],
            mode="lines",
        )
    )
    #Add chin trace
    fig.add_trace(
        go.Scatter(
            x=[0, img_width * scale_factor],
            y=[point_list[3], point_list[3]],
            mode="lines",
        )
    )
    # Configure axes
    fig.update_xaxes(
        visible=False,
        fixedrange=True,
        range=[0, img_width * scale_factor]
    )
    fig.update_yaxes(
        visible=False,
        fixedrange=True,
        range=[0, img_height * scale_factor],
        # the scaleanchor attribute ensures that the aspect ratio stays constant
        scaleanchor="x"
    )
    # Add image
    fig.add_layout_image(
        dict(
            x=0,
            sizex=img_width * scale_factor,
            y=img_height * scale_factor,
            sizey=img_height * scale_factor,
            xref="x",
            yref="y",
            opacity=1.0,
            layer="below",
            sizing="stretch",
            source=image)
    )
    # Configure other layout
    fig.update_layout(
        width=img_width * scale_factor,
        height=img_height * scale_factor,
        showlegend=False,
        margin={"l": 0, "r": 0, "t": 0, "b": 0},
    )
    return fig

def calc_h_stretch(factor, center, dim):
    center = int((center) * dim)
    x = np.arange(dim)
    y_adj = np.square(x - center)*factor
    y_adj[x < center] = y_adj[x < center]*-1
    return x, y_adj


def calc_v_stretch(center, span, dim):
    if center is None:
        center = 0
    factor = 1/(span/5.5)
    def sigmoid(x, factor):
        return 1 / (1 + np.exp(-x*factor))
    center = int((center) * dim)
    x = np.arange(dim)
    y_adj = sigmoid(x - center, factor)
    return x, y_adj


def stretch(img, x, y, yv):
    for color in range(3):
        for row in range(img.shape[0]):
            scale_factor = yv[-row]
            if scale_factor < 0:
                scale_factor = 0
            new_x = x + y*scale_factor
            new_x[new_x > img.shape[0]] = img.shape[0]
            new_x[new_x < 0] = 0
            yi = img[row, :, color]
            f = interpolate.interp1d(new_x, yi, bounds_error=False)
            new_y = f(x)
            img[row, :, color] = new_y
    return img


def find_stretch_factor(target_span, l, r, dim):
    stretch_span = 0
    factor = 0.000000002
    center = np.mean([l, r])
    print('target span', target_span)
    while stretch_span < target_span:
        center = int((center))
        print(center)
        hx = np.arange(dim)
        hy = np.square(hx - center)*factor
        print(hy)
        hy[hx < center] = hy[hx < center]*-1
        new_x = hx + hy
        print(new_x)
        stretch_span = new_x[int(r)] - new_x[int(l)]
        factor += 0.00005
        print(stretch_span)
    return factor


def get_img_out():
    image_filename = 'uploaded_to_app.png'
    encoded_image = base64.b64encode(open(image_filename, 'rb').read()).decode('ascii')
    return 'data:image/png;base64,{}'.format(encoded_image)


def get_proc_img_out():
    image_filename = 'processed_image.png'
    encoded_image = base64.b64encode(open(image_filename, 'rb').read()).decode('ascii')
    return 'data:image/png;base64,{}'.format(encoded_image)


def resize(img, target_x=300):
    x_size = img.shape[1]
    divisor = target_x / x_size
    image_rescaled = rescale(img, divisor, anti_aliasing=False, multichannel=True) * 255
    image_rescaled = image_rescaled.astype(np.uint8)
    return image_rescaled


def get_first_click(point, l=0, r=0, n=0, c=0):
    global point_list
    imax = np.argmax(np.array([l, r, n, c]))
    if imax >= 2:
        point_list[imax] = point[1]
    else:
        point_list[imax] = point[0]


def get_point(points):
    x = points['lassoPoints']['x'][0]
    y = points['lassoPoints']['y'][0]
    return [x, y]


def fix_clicks(l, r, n, c):
    if not l:
        l = 0
    if not r:
        r = 0
    if not n:
        n = 0
    if not c:
        c = 0
    return l, r, n, c


def find_span(size, n, c):
    size_dict = {
        'S': 3,
        'M': 3.2,
        'L': 3.5,
    }
    nose_chin = n - c
    span = nose_chin * size_dict[size]
    return span


@app.callback(Output('dummy3','children'),
              [Input('upload-image','last_modified')])
def clear_outs(_):
    global point_list
    point_list = [0,0,0,0]
    return []


@app.callback([Output('orig_graph', 'figure'),
               Output('dummy2','children')],
              [Input('upload-image', 'contents'),
              Input('rotation','value'),
              Input('padding','value'),
              Input('orig_graph','selectedData'),
              Input('left_button','n_clicks_timestamp'),
              Input('right_button','n_clicks_timestamp'),
              Input('nose_button','n_clicks_timestamp'),
              Input('chin_button','n_clicks_timestamp'),
              ])
def update_output(images, rotation, padding, click_object, l, r, n, c):
    if not images:
        raise PreventUpdate
    l, r, n, c = fix_clicks(l, r, n, c)
    if click_object:
        points = get_point(click_object)
    else:
        points = [0,0]
    get_first_click(points, l, r, n, c)
    image = images.split(',')[1]
    data = decodestring(image.encode('ascii'))
    file_jpgdata = BytesIO(data)
    img = Image.open(file_jpgdata)
    img = np.asarray(img)
    if rotation:
        img = ndimage.rotate(img, rotation, reshape=True)
    img = np.pad(
        img, 
        ((int(img.shape[0]*padding),int(img.shape[0]*padding)),(int(img.shape[1]*padding), int(img.shape[1]*padding)), (0,0)), 
        'constant'
    )
    img = resize(img)
    old_img_array = img.copy()
    proc_img = img.copy()
    proc_img = Image.fromarray(proc_img, 'RGB')
    scale_factor = 650/img.shape[1]
    vcenter = ((point_list[2] + point_list[3])/2)/(img.shape[0]*scale_factor)
    vspan = (point_list[2] - point_list[3])
    if not vspan:
        vspan = 1
    xv, yv = calc_v_stretch(vcenter, vspan, img.shape[0] * scale_factor)
    fig = make_picture(old_img_array, proc_img, xv, yv)
    return [fig, []]


@app.callback([Output('stretched_image', 'src')],
              [Input('process', 'n_clicks_timestamp')],
              [State('upload-image', 'contents'),
              State('rotation','value'),
              State('padding','value'),
              State('orig_graph','selectedData'),
              State('left_button','n_clicks_timestamp'),
              State('right_button','n_clicks_timestamp'),
              State('nose_button','n_clicks_timestamp'),
              State('chin_button','n_clicks_timestamp'),
              State('size','value')
              ])
def update_output(click, images, rotation, padding, click_object, l, r, n, c, size):
    if not click:
        raise PreventUpdate
    if not images:
        raise PreventUpdate
    l, r, n, c = fix_clicks(l, r, n, c)
    if click_object:
        points = get_point(click_object)
    else:
        points = [0,0]
    get_first_click(points, l, r, n, c)
    image = images.split(',')[1]
    data = decodestring(image.encode('ascii'))
    file_jpgdata = BytesIO(data)
    img = Image.open(file_jpgdata)
    img = np.asarray(img)
    if rotation:
        img = ndimage.rotate(img, rotation, reshape=True)
    img = np.pad(
        img, 
        ((int(img.shape[0]*padding),int(img.shape[0]*padding)),(int(img.shape[1]*padding), int(img.shape[1]*padding)), (0,0)), 
        'constant'
    )
    scaled_img = resize(img.copy())
    scale_factor = 650/scaled_img.shape[1]
    vcenter = ((point_list[2] + point_list[3])/2)/(img.shape[0]*scale_factor)
    vspan = (point_list[2] - point_list[3])
    if not vspan:
        vspan = 1
    xv, yv = calc_v_stretch(vcenter, vspan, img.shape[0])
    hcenter = ((point_list[1] + point_list[0])/2)/(scaled_img.shape[1] * scale_factor)
    span = find_span(size, point_list[2]/(scaled_img.shape[0]*scale_factor), point_list[3]/(scaled_img.shape[0]*scale_factor))
    span = span * img.shape[0]
    scaled_l = (point_list[0] / (scaled_img.shape[0]*scale_factor)) * img.shape[0]
    scaled_r = (point_list[1] / (scaled_img.shape[0]*scale_factor)) * img.shape[0]
    stretch_factor = find_stretch_factor(span, scaled_l, scaled_r, img.shape[0])
    hx, hy = calc_h_stretch(stretch_factor, hcenter, img.shape[1])
    proc_img = stretch(img.copy(), hx, hy, yv)
    proc_img = Image.fromarray(proc_img, 'RGB')
    proc_img.save('processed_image.png')
    return [get_proc_img_out()]



@app.server.route('/downloads')
def view_method():
    return send_file(
        'processed_image.png',
        mimetype='image/PNG', 
        attachment_filename='processed{}'.format(datetime.datetime.now()),
        as_attachment=True
    )


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8080, debug=True)